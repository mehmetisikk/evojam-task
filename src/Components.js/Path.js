import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { ImHome } from "react-icons/im";

const Path = ({ pathes }) => {
  const navigate = useNavigate();
  console.log(pathes);
  return (
    <div className="flex flex-row justify-start items-center gap-5">
      <ImHome
        color={"darkblue"}
        onClick={() => {
          navigate("/");
        }}
      ></ImHome>
      <p>
        {pathes.map((path, index) => (
          <Link
            to={
              path !== "root"
                ? pathes.slice(1, pathes.indexOf(path) + 1).join("/")
                : "/"
            }
            key={index}
            className="hover:underline hover:text-blue-600"
          >
            /{path}
          </Link>
        ))}
      </p>
    </div>
  );
};

export default Path;
